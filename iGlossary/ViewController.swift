//
//  ViewController.swift
//  iGlossary
//
//  Created by A Khairini on 13/04/20.
//  Copyright © 2020 Infinite Learning ADA. All rights reserved.
//

import UIKit

struct Glossaries {
    let terms: String
    let definitions: String
    let links: String
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    
    var termArray: [Glossaries] = [
        Glossaries(terms: "Algorithm", definitions: "A programming algorithm is a computer procedure that is a lot like a recipe (called a procedure) and tells your computer precisely what steps to take to solve a problem or reach a goal", links: "https://www.khanacademy.org/computing/computer-science/algorithms/intro-to-algorithms/v/what-are-algorithms"),
        Glossaries(terms: "Array", definitions: "Arrays are containers that hold variables; they're used to group together similar variables. You can think of arrays like shelves at a pet store. The array would be the shelf, and the animals in cages are the variables inside.", links: "https://www.youtube.com/watch?v=NptnmWvkbTw"),
        Glossaries(terms: "Bug", definitions: "an error, flaw or fault in a computer program or system that causes it to produce an incorrect or unexpected result, or to behave in unintended ways.", links: "https://www.youtube.com/watch?v=kRL6hjWOKWI"),
        Glossaries(terms: "Syntax", definitions: "Set of rules that defines the combinations of symbols that are considered to be a correctly structured document or fragment in that language", links: "https://dept-info.labri.fr/~strandh/Teaching/MTP/Common/Strandh-Tutorial/syntax.html")
    ]
    
    var display = [Glossaries]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        display = termArray
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("data \(display.count)")
        return display.count
    }
    
    //Asks the data source for a cell to insert in a particular location of the table view.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  UITableViewCell()
        cell.textLabel?.text = display[indexPath.row].terms
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showdefinition", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DefinitionScreen {
            destination.term = display[(tableView.indexPathForSelectedRow?.row)!]
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var newlist = [Glossaries]()
        if(searchText.count > 0){
            for item in termArray{
                if ((item.terms.contains(searchText))) {
                    newlist.append(item)
                    print(item.terms)
                }
            }
            display = newlist
            print(display.count)
            tableView.reloadData()
        }else{
            display = [Glossaries]()
            display = termArray
            tableView.reloadData()
        }
        
    }
    
}

