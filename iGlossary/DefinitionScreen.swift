import UIKit

class DefinitionScreen: UIViewController {
    
    @IBOutlet weak var termTitle: UILabel!
    @IBOutlet weak var termDefinition: UITextView!
    @IBOutlet weak var learnMore: UILabel!
    
    var term: Glossaries?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termTitle.text = "\((term?.terms)!)"
        termTitle.lineBreakMode = .byWordWrapping
        termTitle.numberOfLines = 0
        termDefinition.text = "\((term?.definitions)!)"
        learnMore.text = "Learn More about \((term?.terms)!) here:"
    }
}
